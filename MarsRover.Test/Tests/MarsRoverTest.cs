﻿using System;
using MarsRover.App.Enum;
using MarsRover.App.Library;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MarsRover.Test.Tests
{
    /// <summary>
    /// Test Class for Rover
    /// </summary>
    [TestClass]
    public class MarsRoverTest
    {
        /// <summary>
        /// Test method for First Rover check output
        /// </summary>
        [TestMethod]
        public void FirstRoverCheckOutput()
        {
            var plateauOne = new Plateau(new Position(5, 5));
            var firstRover = new Rover(plateauOne, new Position(1, 2), Directions.N);
            firstRover.Process("LMLMLMLMM");
            Assert.AreEqual(firstRover.ToString(), "1 3 N");
        }

        /// <summary>
        /// Test method for Second Rover check output
        /// </summary>
        [TestMethod]
        public void SecondRoverCheckOutput()
        {
            var plateauTwo = new Plateau(new Position(5, 5));
            var secondRover = new Rover(plateauTwo, new Position(3, 3), Directions.E);
            secondRover.Process("MMRMMRMRRM");
            Assert.AreEqual(secondRover.ToString(), "5 1 E");
        }

        /// <summary>
        /// Test method for incorrect input
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void IncorrectInput()
        {
            var plateauOne = new Plateau(new Position(5, 5));
            var firstRover = new Rover(plateauOne, new Position(1, 2), Directions.N);
            firstRover.Process("LMAMMM");
        }
    }
}
