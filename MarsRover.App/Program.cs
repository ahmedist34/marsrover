﻿using System;
using MarsRover.App.Enum;
using MarsRover.App.Library;

namespace MarsRover.App
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("__Test Input:__");
            Console.WriteLine();
            Console.WriteLine("5 5");
            Console.WriteLine("1 2 N");
            Console.WriteLine("LMLMLMLMM");
            Console.WriteLine();
            Console.WriteLine("3 3 E");
            Console.WriteLine("MMRMMRMRRM");
            Console.WriteLine();

            //First rover 
            var plateauOne = new Plateau(new Position(5, 5));
            var firstRover = new Rover(plateauOne, new Position(1, 2), Directions.N);
            firstRover.Process("LMLMLMLMM");

            //Second rover 
            var plateauTwo = new Plateau(new Position(5, 5));
            var secondRover = new Rover(plateauTwo, new Position(3, 3), Directions.E);
            secondRover.Process("MMRMMRMRRM");

            Console.WriteLine("__Expected Output:__");
            Console.WriteLine();
            Console.WriteLine(firstRover.ToString());
            Console.WriteLine(secondRover.ToString());
            Console.WriteLine();

            Console.ReadLine();
        }
    }
}
