﻿namespace MarsRover.App.Enum
{
    /// <summary>
    /// Directions enum
    /// </summary>
    public enum Directions
    {
        N = 1,
        E = 2,
        S = 3,
        W = 4
    }
}
