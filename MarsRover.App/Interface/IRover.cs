﻿using MarsRover.App.Enum;
using MarsRover.App.Library;

namespace MarsRover.App.Interface
{
    /// <summary>
    /// Rover Interface
    /// </summary>
    public interface IRover
    {
        IPlateau RoverPlateau { get; set; }
        IPosition RoverPosition { get; set; }
        Directions RoverDirection { get; set; }
        void Process(string commands);
        string ToString();
    }
}
