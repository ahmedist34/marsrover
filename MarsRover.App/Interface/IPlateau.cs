﻿using MarsRover.App.Library;

namespace MarsRover.App.Interface
{
    /// <summary>
    /// Plateau Interface
    /// </summary>
    public interface IPlateau
    {
        Position PlateauPosition { get; }
    }
}
