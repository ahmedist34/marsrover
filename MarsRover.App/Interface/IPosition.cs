﻿namespace MarsRover.App.Interface
{
    /// <summary>
    /// Position Interface
    /// </summary>
    public interface IPosition
    {
        int XCoordinate { get; set; }
        int YCoordinate { get; set; }
    }
}
