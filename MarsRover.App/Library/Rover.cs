﻿using System;
using MarsRover.App.Enum;
using MarsRover.App.Interface;

namespace MarsRover.App.Library
{
    /// <summary>
    /// Rover class structure
    /// </summary>
    public class Rover : IRover
    {
        public IPlateau RoverPlateau { get; set; }
        public IPosition RoverPosition { get; set; }
        public Directions RoverDirection { get; set; }


        public Rover(IPlateau roverPlateau, IPosition roverPosition, Directions roverDirection)
        {
            RoverPlateau = roverPlateau;
            RoverPosition = roverPosition;
            RoverDirection = roverDirection;
        }

        public void Process(string commands)
        {
            foreach (var command in commands)
            {
                switch (command)
                {
                    case ('L'):
                        TurnLeft();
                        break;
                    case ('R'):
                        TurnRight();
                        break;
                    case ('M'):
                        Move();
                        break;
                    default:
                        throw new ArgumentException($"Invalid value: {command}");
                }
            }
        }

        private void TurnLeft()
        {
            RoverDirection = (RoverDirection - 1) < Directions.N ? Directions.W : RoverDirection - 1;
        }

        private void TurnRight()
        {
            RoverDirection = (RoverDirection + 1) > Directions.W ? Directions.N : RoverDirection + 1;
        }

        private void Move()
        {
            switch (RoverDirection)
            {
                case Directions.N:
                    RoverPosition.YCoordinate++;
                    break;
                case Directions.E:
                    RoverPosition.XCoordinate++;
                    break;
                case Directions.S:
                    RoverPosition.YCoordinate--;
                    break;
                case Directions.W:
                    RoverPosition.XCoordinate--;
                    break;
            }
        }

        public override string ToString()
        {
            return $"{RoverPosition.XCoordinate} {RoverPosition.YCoordinate} {RoverDirection}";
        }
    }
}
