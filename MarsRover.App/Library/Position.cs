﻿using MarsRover.App.Interface;

namespace MarsRover.App.Library
{
    /// <summary>
    /// Position class structure
    /// </summary>
    public class Position : IPosition
    {
        public int XCoordinate { get; set; }
        public int YCoordinate { get; set; }

        public Position(int x, int y)
        {
            XCoordinate = x;
            YCoordinate = y;
        }
    }
}
