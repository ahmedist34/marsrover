﻿using MarsRover.App.Interface;

namespace MarsRover.App.Library
{
    /// <summary>
    /// Plateau class structure
    /// </summary>
    public class Plateau : IPlateau
    {
        public Position PlateauPosition { get; private set; }

        public Plateau(Position position)
        {
            PlateauPosition = position;
        }
    }
}
